function [ pruned_matrix ] = prune_equilibria( eq_struct )
%PRUNE_EQUILIBRIA This function takes a matrix and removes the row associated
%with any entries that are not equal to or greater than zero. It is
%intended to work with the struct returned by get_equilibrium.

% INPUT
%   eq_struct: struct that contains three fields:
%       x: the x values associated with all equilibria found
%       y: the y values associated with all equilibria found
%       z: the z values associated with all equilibria found

% OUTPUT
%   pruned_matrix: matrix, each row of the matrix represents one equilibrium
%       point, with the first column containg x, the second column y, and
%       the third column z. Only equilibria equal to or greater than zero
%       will be saved in the matrix. If there are none, and empty matrix is
%       returned.

% CALLED BY:
%   stability_analysis
%   stability_analysis_on_a

% CHANGE LOG:
%   I forgot to set this to prune out imaginary equilibria, now it does
%   that. Vera 3/4/17.

num_rows = length(eq_struct.x);

% Preallocate
eq_matrix = nan(num_rows, 3);

row_counter = 0;
for i = 1:num_rows

    % Select only equilibria that are REAL and POSITIVE
    if (eq_struct.x(i) >= 0) && (eq_struct.y(i) >= 0) && (eq_struct.z(i) >= 0) &&...
            isreal(eq_struct.x(i)) && isreal(eq_struct.y(i)) && isreal(eq_struct.z(i))
        
        row_counter = row_counter + 1;
        
        % Save the equilibrium if all entries are >= 0
        eq_matrix(i, 1) = eq_struct.x(i);
        eq_matrix(i, 2) = eq_struct.y(i);
        eq_matrix(i, 3) = eq_struct.z(i);
    end
end

% Row_counter tells us how many rows are not all NaNs
if row_counter == 0
    pruned_matrix = [];
    
else
    % Preallocate a matrix
    pruned_matrix = nan(row_counter, 3);
    
    prune_count = 1;
    for k = 1:num_rows % Rows in the un-pruned matrix
        if ~isnan(eq_matrix(k, 1)) % Indicates a non-nan row
            pruned_matrix(prune_count, :) = eq_matrix(k, :);
            prune_count = prune_count + 1;
        end
    end
end
    
end

