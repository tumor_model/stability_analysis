% This program assumes that you ran stability_analysis.m, and saved the
% c_val_array and notable_c_values. These should be loaded in the
% workspace. This program will go through those structs and make vectors
% for plotting.

y_value_vector = nan(1, 4 * length(c_val_array));
c_value_vector = nan(1, length(y_value_vector));
% One value of c may have up to 4 y equilibria

counter = 1;
for i = 1:length(c_val_array)
    c_value = c_val_array{i}.c_val;
    y_values = c_val_array{i}.y_equilibria;
    for j = 1:length(y_values)
        if isreal(y_values(j))
            c_value_vector(counter) = c_value;
            y_value_vector(counter) = y_values(j);
            counter = counter + 1;
        end
    end
end

plotter_vector = log(y_value_vector);
plot(c_value_vector, plotter_vector, 'p')
ylim([8, 21])
title('Equilibrium Tumor Cells Over Agenticity')
ylabel('Log of Number of Tumor Cells (log(y))')
xlabel('Agenticity (c)')
