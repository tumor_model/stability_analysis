function [ eq_struct ] = get_equilibrium( tumor_p )
%GET_EQUILIBRIUM This function gets the equilibria for the constants
%provided in tumor_p

% INPUT:
%   tumor_p: struct containing all other constants for the model

% OUTPUT:
%   eq_struct: struct containing all equilibrium points found for the
%       system. eq_struct.x contains all the x values, eq_struct.y contains
%       the corresponding y values, and so on.

% CALLED BY:
%   stability_analysis
%   stability_analysis_on_a

% CHANGE LOG:
%   Modified to get all paramters from the tumor_p struct. Vera 3/4/17.   

% Values from Table 1
mu_2 = tumor_p.mu_2;
p_1 = tumor_p.p_1;
g_1 = tumor_p.g_1;
g_2 = tumor_p.g_2;
r_2 = tumor_p.r_2;
b = tumor_p.b;
a = tumor_p.a;
mu_3 = tumor_p.mu_3;
p_2 = tumor_p.p_2;
g_3 = tumor_p.g_3;
c = tumor_p.c; % c now in the tumor_p struct
s_1 = tumor_p.s_1;
s_2 = tumor_p.s_2;

% This is the way to get MATLAB to solve this. It works, don't break it!
syms x
syms y
syms z

eq_struct = vpasolve(0 == c * y - mu_2 * x + (p_1 * x * z / (g_1 + z)) + s_1,...
    0 == r_2 * y * (1 - b * y) - a * x * y / (g_2 + y),...
         0 == p_2 * x * y / (g_3 + y) - mu_3 * z + s_2, x, y, z);

end
